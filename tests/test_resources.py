import unittest
from pmtoolbox import app, db
from app.models.customer import CustomerModel
from app.models.cr import CRModel
from datetime import datetime as dt
import json


class ModelsTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.app = app.test_client()
        db.create_all()

        # Disable sending emails during unit testing
        self.assertEqual(app.debug, False)

    # executed after each test
    def tearDown(self):
        db.drop_all()


###############
#### tests ####
###############


    def test_getting_non_existing_cr(self):
        response = self.app.get('/api/v1/cr/666', follow_redirects=True)
        json = response.get_json()
        self.assertIn("CR '666' not found", str(json))

    def test_getting_existing_cr(self):
        customer = CustomerModel('CABP', '', '', '', 16.0)
        customer.save_or_update()
        cr = CRModel('43', 'Rozbudowa eksportów do DWH o kolumnę identyfikującą rodzaj opłat', dt.now(
        ), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 1200.0, 168.0, 1200.0)
        cr.save_or_update()
        response = self.app.get('/api/v1/cr/43', follow_redirects=True)
        json = response.get_json()
        self.assertIn("CABP", str(json))

    def test_creating_cr(self):
        customer = CustomerModel('CABP', '', '', '', 16.0)
        customer.save_or_update()
        cr_data = {
            'title': 'Rozbudowa eksportów do DWH o kolumnę identyfikującą rodzaj opłat',
            'issue_date': dt.now().strftime('%Y%m%d'),
            'customer_id': customer.id,
            'net_value': 44600.0,
            'net_service_charge': 7136.0,
            'net_service_base': 44600.0,
            'file_id': 'f581fc0e-74f4-420b-a565-bb2b08f354c2',
            'folder_id': 'c2e5cc99-68b5-4e77-9d1c-adad1b1454e8',
            'properties_bag': {'WARTOSC_ZAMOWIENIA_BRUTTO': 'WARTOSC_ZAMOWIENIA_NETTO*1.23'}
        }
        response = self.app.post('/api/v1/cr/44',
                                 data=json.dumps(cr_data),
                                 content_type='application/json')
        self.assertEqual(201, response.status_code)
        self.assertIsNotNone(response.get_json()[
                             'id'], 'Created CR should have an id')

    def test_rejection_creating_existing_cr(self):
        customer = CustomerModel('SCM', '', '', '', 14.0)
        customer.save_or_update()
        cr = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8",
                     customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO': 'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr.save_or_update()
        cr_data = {
            'title': 'Rozbudowa eksportów do DWH o kolumnę identyfikującą rodzaj opłat',
            'issue_date': dt.now().strftime('%Y%m%d'),
            'customer_id': customer.id,
            'net_value': 44600.0,
            'net_service_charge': 7136.0,
            'net_service_base': 44600.0,
            'properties_bag': {'WARTOSC_ZAMOWIENIA_BRUTTO': 'WARTOSC_ZAMOWIENIA_NETTO*1.23'}
        }
        response = self.app.post('/api/v1/cr/44',
                                 data=json.dumps(cr_data),
                                 content_type='application/json')
        self.assertEqual(409, response.status_code)
        self.assertIn("already exists", response.get_json()['message'])

    def test_updating_cr(self):
        customer = CustomerModel('SCM', '', '', '', 14.0)
        customer.save_or_update()
        cr = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8",
                     customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO': 'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr.save_or_update()
        cr_data = {
            'title': 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur',
            'issue_date': cr.issue_date.strftime('%Y%m%d'),
            'customer_id': customer.id,
            'net_value': 44600.0,
            'net_service_charge': 7136.0,
            'net_service_base': 20600.0,
            'folder_id': '71f37b21-08be-4f85-9180-41ffd61fcda3',
            'file_id': '71f37b21-08be-4f85-9180-41ffd61fcda3',
            'properties_bag': {'ZAMAWIAJACY': 'Piotr Wójcik'}
        }
        response = self.app.put('/api/v1/cr/44',
                                data=json.dumps(cr_data),
                                content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.get_json()[
                         'net_value'], 44600, f'Updated value is {cr_data["net_value"]}')

    def test_deleting_existing_cr(self):
        customer = CustomerModel('SCM', '', '', '', 14.0)
        customer.save_or_update()
        cr = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 
                     customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO': 'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr.save_or_update()
        from_db = CRModel.find_by_number('44')
        self.assertIsNotNone(from_db)
        response = self.app.delete('/api/v1/cr/44')
        self.assertEqual(200, response.status_code)
        self.assertIn("deleted", response.get_json()['message'])

    def test_deleting_non_existing_cr(self):
        response = self.app.delete('/api/v1/cr/666')
        self.assertEqual(404, response.status_code)
        self.assertIn("not found", response.get_json()['message'])

    def test_updating_non_existing_cr(self):
        customer = CustomerModel('SCM', '', '', '', 14.0)
        customer.save_or_update()
        cr_data = {
            'title': 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur',
            'issue_date': dt.now().strftime('%Y%m%d'),
            'customer_id': customer.id,
            'net_value': 44600.0,
            'net_service_charge': 7136.0,
            'net_service_base': 20600.0,
            'file_id': 'f581fc0e-74f4-420b-a565-bb2b08f354c2',
            'folder_id': 'f581fc0e-74f4-420b-a565-bb2b08f354c2',
            'properties_bag': {'ZAMAWIAJACY': 'Piotr Wójcik'}
        }
        response = self.app.put('/api/v1/cr/44',
                                data=json.dumps(cr_data),
                                content_type='application/json')
        self.assertEqual(201, response.status_code)
        self.assertEqual(response.get_json()[
                         'net_value'], 44600, f'Updated value is {cr_data["net_value"]}')

    def test_deleting_non_existing_cr(self):
        response = self.app.delete('/api/v1/cr/666')
        self.assertEqual(404, response.status_code)
        self.assertIn("not found", response.get_json()['message'])

    def test_updating_existing_cr_without_required_data(self):
        customer = CustomerModel('SCM', '', '', '', 14.0)
        customer.save_or_update()
        cr = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8",
                     customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO': 'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr.save_or_update()
        cr_data = {
            'title': 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur',
            'issue_date': cr.issue_date.strftime('%Y%m%d'),
            'customer_id': customer.id,
            'net_value': 44600.0,
            'net_service_charge': 7136.0,
            'net_service_base': 20600.0,
            'file_id': '71f37b21-08be-4f85-9180-41ffd61fcda3',
            'properties_bag': {'ZAMAWIAJACY': 'Piotr Wójcik'}
        }
        response = self.app.put('/api/v1/cr/44',
                                data=json.dumps(cr_data),
                                content_type='application/json')
        self.assertEqual(400, response.status_code)
        self.assertIn('folder_id is required', str(response.get_json()['message']))

    def test_getting_all_crs_for_customer(self):
        customer = CustomerModel('SCM', "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94",
                                 "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 14.0)
        customer.save_or_update()
        cr = CRModel('43', 'Rozbudowa eksportów do DWH o kolumnę identyfikującą rodzaj opłat', dt.now(
        ), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 1200.0, 168.0, 1200.0)
        cr.save_or_update()
        cr2 = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 
                      customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO': 'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr2.save_or_update()
        response = self.app.get('/api/v1/customer/SCM/crs',
                                content_type='application/json')
        self.assertEqual(200, response.status_code)
        crs = response.get_json()['crs']
        self.assertEqual(2, len(crs), f"There are two CRs for {customer.name}")
        customer2 = CustomerModel('CABP', "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94",
                                  "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 14.0)
        customer2.save_or_update()
        response = self.app.get('/api/v1/customer/CABP/crs',
                                content_type='application/json')
        self.assertEqual(200, response.status_code)
        crs = response.get_json()['crs']
        self.assertEqual(0, len(crs), f"There are no CRs for {customer2.name}")

    def test_getting_non_existing_customer(self):
        response = self.app.get('/api/v1/customer/CABP',
                                content_type='application/json')
        self.assertEqual(404, response.status_code)
        self.assertIn('not found', response.get_json()['message'])

    def test_getting_existing_customer(self):
        customer = CustomerModel('SCM', '', '', '', 14.0)
        customer.save_or_update()
        response = self.app.get('/api/v1/customer/SCM',
                                content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertIn('SCM', response.get_json()['name'])

    def test_creating_customer(self):
        customer_date = {
            'cr_template': '',
            'protocol_template': '',
            'cr_folder': '',
            'service_ratio': 16.0
        }
        response = self.app.post('/api/v1/customer/CABP',
                                 data=json.dumps(customer_date),
                                 content_type='application/json')
        self.assertEqual(201, response.status_code)
        self.assertEqual('CABP', response.get_json()['name'])

    def test_creating_existing_customer(self):
        customer_date = {
            'cr_template': '',
            'protocol_template': '',
            'cr_folder': '',
            'service_ratio': 16.0
        }
        response = self.app.post('/api/v1/customer/CABP',
                                 data=json.dumps(customer_date),
                                 content_type='application/json')
        self.assertEqual(201, response.status_code)
        response = self.app.post('/api/v1/customer/CABP',
                                 data=json.dumps(customer_date),
                                 content_type='application/json')
        self.assertEqual(409, response.status_code)
        self.assertIn('already exists', response.get_json()['message'])

    def test_updating_existing_customer(self):
        customer = CustomerModel('SCM', '', '', '', 14.0)
        customer.save_or_update()
        response = self.app.get('/api/v1/customer/SCM',
                                content_type='application/json')
        customer_date = {
            'cr_template': '',
            'protocol_template': '',
            'cr_folder': '',
            'service_ratio': 16.0
        }
        response = self.app.put('/api/v1/customer/SCM',
                                data=json.dumps(customer_date),
                                content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(16.0, response.get_json()['service_ratio'])

    def test_updating_non_existing_customer(self):
        customer_date = {
            'cr_template': '',
            'protocol_template': '',
            'cr_folder': '',
            'service_ratio': 16.0
        }
        response = self.app.put('/api/v1/customer/SCM',
                                data=json.dumps(customer_date),
                                content_type='application/json')
        self.assertEqual(201, response.status_code)
        self.assertEqual(16.0, response.get_json()['service_ratio'])

    def test_deleting_customer(self):
        customer = CustomerModel('SCM', '', '', '', 14.0)
        customer.save_or_update()
        response = self.app.delete('/api/v1/customer/SCM', content_type='application/json')
        self.assertIn('deleted', response.get_json()['message'])
        self.assertIsNone(CustomerModel.find_by_name('SCM'))

    def test_cascade_on_deleting_customer(self):
        customer = CustomerModel('SCM', '', '', '', 14.0)
        customer.save_or_update()
        cr = CRModel('43', 'Rozbudowa eksportów do DWH o kolumnę identyfikującą rodzaj opłat', dt.now(
        ), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 1200.0, 168.0, 1200.0)
        cr.save_or_update()
        response = self.app.get('/api/v1/customer/SCM', content_type='application/json')
        self.assertEqual(len(response.get_json()['crs']), 1)
        response = self.app.delete('/api/v1/customer/SCM', content_type='application/json')
        self.assertIn('deleted', response.get_json()['message'])
        self.assertIsNone(CRModel.find_by_number('43'))

    def test_deleting_non_existing_customer(self):
        response = self.app.delete('/api/v1/customer/I_DONT_EXIST', content_type='application/json')
        self.assertIn('not found', response.get_json()['message'])

    def test_getting_all_customers(self):
        customer = CustomerModel('SCM', '', '', '', 14.0)
        customer.save_or_update()
        customer2 = CustomerModel('CABP', '', '', '', 16.0)
        customer2.save_or_update()
        response = self.app.get('/api/v1/customers', content_type='application/json')
        self.assertEqual(len(response.get_json()['customers']), 2, 'There are two customers')

if __name__ == "__main__":
    unittest.main()
