import unittest

from pmtoolbox import app,db
 
class BasicTests(unittest.TestCase):
 
    ############################
    #### setup and teardown ####
    ############################
 
    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.app = app.test_client()
        db.create_all()
 
        # Disable sending emails during unit testing
        self.assertEqual(app.debug, False)
 
    # executed after each test
    def tearDown(self):
        db.drop_all()
 
 
###############
#### tests ####
###############
 
    def test_main_page(self):
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(200, response.status_code, "Response code should be 200")
        self.assertIn(b"PM ToolBox", response.data, "/ has title 'PM ToolBox'" )
        self.assertEqual('text/html', response.mimetype)
 
 
if __name__ == "__main__":
    unittest.main()