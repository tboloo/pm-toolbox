import unittest
from pmtoolbox import app,db
from app.models.cr import CRModel
from app.models.customer import CustomerModel
from datetime import datetime as dt

class ModelsTests(unittest.TestCase):
 
    ############################
    #### setup and teardown ####
    ############################
 
    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.app = app.test_client()
        db.create_all()
 
        # Disable sending emails during unit testing
        self.assertEqual(app.debug, False)
 
    # executed after each test
    def tearDown(self):
        db.session.rollback()
        db.drop_all()
 
 
###############
#### tests ####
###############
 
    def test_cr_model(self):
        customer = CustomerModel('CABP', "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94", "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 16.0)
        customer.save_or_update()
        self.assertIsNone(customer.last_cr_number)
        cr = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO':'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr.save_or_update()
        self.assertIsNotNone(customer.last_cr_number)
        self.assertIsNotNone(cr.id, 'Object gets id after saving')

    def test_finding_cr_by_number(self):
        customer = CustomerModel('CABP', "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94", "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 16.0)
        customer.save_or_update()
        cr = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO':'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr.save_or_update()
        from_db = CRModel.find_by_number('44')
        self.assertIsNotNone(from_db, 'Object is retrived from db')
        self.assertEqual(cr.title, from_db.title)

    def test_deleting_cr_from_database(self):
        customer = CustomerModel('CABP', "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94", "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 16.0)
        customer.save_or_update()
        cr = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO':'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr.save_or_update()
        from_db = CRModel.find_by_number('44')
        from_db.delete()
        deleted = CRModel.find_by_number('44')
        self.assertIsNone(deleted)

    def test_cr_json_representation(self):
        cr = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 666, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO':'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        json = cr.toJson()
        self.assertIn("'title': 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur'", str(json))
        representation = cr.__repr__()
        self.assertIn("issued on", representation)

    def test_adding_custom_properties_to_cr(self):
        customer = CustomerModel('CABP', "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94", "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 16.0)
        customer.save_or_update()
        cr = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO':'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr['REDMINE_ID'] = '#30706'
        cr.save_or_update()
        from_db = CRModel.find_by_number('44')
        self.assertEqual('#30706', from_db['REDMINE_ID'])

    def test_getting_all_crs_issued_by_john_doe(self):
        customer = CustomerModel('SCM', "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94", "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 14.0)
        customer.save_or_update()
        cr = CRModel('43', 'Rozbudowa eksportów do DWH o kolumnę identyfikującą rodzaj opłat', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 1200.0, 168.0, 1200.0)
        cr['ISSUED_BY'] = 'John Doe'
        cr.save_or_update()
        cr2 = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO':'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr2['ISSUED_BY'] = 'John Doe'
        cr2.save_or_update()
        cr3 = CRModel('45', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO':'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr3['ISSUED_BY'] = 'John Wick'
        cr3.save_or_update()
        crs = CRModel.query.filter(CRModel.with_properties('ISSUED_BY','John Doe')).all()
        self.assertEqual(2, len(crs), "There are two CRs issued by John Doe")

    def test_verify_foreign_key_violation_for_non_existing_customer(self):
        with self.assertRaisesRegex(Exception, 'FOREIGN KEY'):
            cr = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 666, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO':'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
            cr.save_or_update()

    def test_getting_crs_for_customer(self):
        customer = CustomerModel('SCM', "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94", "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 14.0)
        customer.save_or_update()
        cr = CRModel('43', 'Rozbudowa eksportów do DWH o kolumnę identyfikującą rodzaj opłat', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 1200.0, 168.0, 1200.0)
        cr.save_or_update()
        cr2 = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO':'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr2.save_or_update()
        customer.save_or_update()
        self.assertEqual(2, len(customer.crs.all()), f'There are two CRs for {customer.name}')
        json = customer.toJson()
        self.assertRegexpMatches(str(json), "43|44|\bkolumnę\b|\bobsługa\b")

    def test_getting_last_cr_number_for_customer(self):
        customer = CustomerModel('SCM', "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94", "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 14.0)
        customer.save_or_update()
        cr = CRModel('43', 'Rozbudowa eksportów do DWH o kolumnę identyfikującą rodzaj opłat', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 1200.0, 168.0, 1200.0)
        cr.save_or_update()
        self.assertEqual(43, customer.last_cr_number)
        cr2 = CRModel('44', 'Modyfikacja komunikacji z LEO FK - obsługa API do księgowania korekt faktur', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer.id, 12300.00, 1722.0, 12300.00, {'WARTOSC_ZAMOWIENIA_BRUTTO':'WARTOSC_ZAMOWIENIA_NETTO*1.23'})
        cr2.save_or_update()
        self.assertEqual(44, customer.last_cr_number)
        customer2 = CustomerModel('CABP', "", "", "", 16.0)
        customer2.save_or_update()
        cr3 = CRModel('37', 'P1045 CMO Amendment to the Act (AttA)', dt.now(), "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", customer2.id, 1200.0, 168.0, 1200.0)
        cr3.save_or_update()
        self.assertEqual(37, customer2.last_cr_number)

    def test_finding_customer_in_db(self):
        customer = CustomerModel('SCM', "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94", "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 14.0)
        customer.save_or_update()
        from_db = CustomerModel.find_by_name('SCM')
        self.assertIsNotNone(from_db)
        self.assertEqual(14.0, from_db.service_ratio)

    def test_deleting_customer_from_db(self):
        name = 'SCM'
        customer = CustomerModel(name, "ade1ca48-40f1-4e59-9c20-f2ecaf34ed94", "f581fc0e-74f4-420b-a565-bb2b08f354c2", "c2e5cc99-68b5-4e77-9d1c-adad1b1454e8", 14.0)
        customer.save_or_update()
        from_db = CustomerModel.find_by_name(name)
        from_db.delete()
        deleted = CustomerModel.find_by_name(name)
        self.assertIsNone(deleted)
 
if __name__ == "__main__":
    unittest.main()