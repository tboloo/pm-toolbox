from app import app, db, api
from app.models.cr import CRModel
from app.resources.cr import CR, CRList
from app.resources.customer import Customer, CustomerList
from app.models.cr import CRModel
from app.models.customer import CustomerModel
from flask import render_template
from app.utils.last_modified import last_modified

@app.route('/')
def index():
    return render_template('index.html', last_modified=last_modified('app/static'))

@app.errorhandler(404)
def page_not_found(e):
    return render_template('index.html', last_modified=last_modified('app/static')), 200

api.add_resource(CR, '/api/v1/cr/<string:number>')
api.add_resource(CRList, '/api/v1/customer/<string:name>/crs')
api.add_resource(Customer, '/api/v1/customer/<string:name>')
api.add_resource(CustomerList, '/api/v1/customers')