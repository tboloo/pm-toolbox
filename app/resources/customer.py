from flask_restful import Resource, reqparse
from app.models.customer import CustomerModel

class Customer(Resource):
    
    parser = reqparse.RequestParser()
    parser.add_argument('cr_template', type=str, required=True,
                        help="CR template is required")
    parser.add_argument('protocol_template', type=str, required=True,
                        help="Protocol template is required") 
    parser.add_argument('cr_folder', type=str, required=True,
                        help="customer folder is required")
    parser.add_argument('service_ratio', type=float, required=True,
                        help="service ratio is required")                     

    def get(self, name):
        customer = CustomerModel.find_by_name(name)
        if customer:
            return customer.toJson(), 200
        return {'message' : f"Customer '{name}' not found"}, 404

    def post(self, name):
        customer = CustomerModel.find_by_name(name)
        if customer:
            return {'message': { 'name' : f'Customer {name} already exists'}}, 409
        data = Customer.parser.parse_args()
        data['name'] = name
        customer = CustomerModel(**data)
        customer.save_or_update()
        return customer.toJson(), 201

    def put(self, name):
        customer = CustomerModel.find_by_name(name)
        data = Customer.parser.parse_args()
        if customer:
            customer.cr_template = data['cr_template']
            customer.protocol_template = data['protocol_template']
            customer.cr_folder = data['cr_folder']
            customer.service_ratio = data['service_ratio']
            customer.save_or_update()
            return customer.toJson(), 200
        data['name'] = name
        customer = CustomerModel(**data)
        customer.save_or_update()
        return customer.toJson(), 201

    def delete(self, name):
        customer = CustomerModel.find_by_name(name)
        if customer:
            customer.delete()
            return {'message': f'Customer {name} deleted'}, 200
        return {'message' : f"Customer '{name}' not found"}, 404
        
class CustomerList(Resource):
    def get(self):
        return {'customers': [customer.toJson() for customer in CustomerModel.query.all()]}