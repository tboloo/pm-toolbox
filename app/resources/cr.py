from flask_restful import Resource, reqparse
from app.models.cr import CRModel
from app.models.customer import CustomerModel
import datetime

class CR(Resource):
    
    parser = reqparse.RequestParser()
    parser.add_argument('title', type=str, required=True,
                        help="CR title is required")
    parser.add_argument('issue_date', type=str, required=True,
                        help="Issue date is required") 
    parser.add_argument('customer_id', type=int, required=True,
                        help="customer_id is required")
    parser.add_argument('net_value', type=float, required=True,
                        help="net_value is required")                     
    parser.add_argument('net_service_base', type=float, required=True,
                        help="net_service_base is required")
    parser.add_argument('net_service_charge', type=float, required=True,
                        help="net_service_charge is required")
    parser.add_argument('file_id', type=str, required=True,
                        help="file_id is required")
    parser.add_argument('folder_id', type=str, required=True,
                        help="folder_id is required")
    parser.add_argument('properties_bag', type=dict, required=False)

    def get(self, number):
        cr = CRModel.find_by_number(number)
        if cr:
            return cr.toJson(), 200
        else:
            return {'message' : f"CR '{number}' not found"}, 404

    def post(self, number):
        cr = CRModel.find_by_number(number)
        if cr:
            return {'message': f"CR {number} already exists"}, 409
        data = CR.parser.parse_args()
        data['number'] = number
        data['issue_date'] = datetime.datetime.strptime(data['issue_date'], "%Y%m%d").date()
        cr = CRModel(**data)
        cr.save_or_update()
        return cr.toJson(), 201

    def put(self, number):
        cr = CRModel.find_by_number(number)
        data = CR.parser.parse_args()
        data['issue_date'] = datetime.datetime.strptime(data['issue_date'], "%Y%m%d").date()
        if cr:
            cr.title = data['title']
            cr.issue_date = data['issue_date']
            cr.net_value = data['net_value']
            cr.file_id = data['file_id']
            cr.folder_id = data['folder_id']
            cr.net_service_base = data['net_service_base']
            cr.net_service_charge = data['net_service_charge']
            cr.properties_bag = data['properties_bag']
            cr.save_or_update()
            return cr.toJson(), 200
        data['number'] = number
        cr = CRModel(**data)
        cr.save_or_update()
        return cr.toJson(), 201

    def delete(self, number):
        cr = CRModel.find_by_number(number)
        if cr:
            cr.delete()
            return {'message': f'CR {number} deleted'}, 200
        return {'message' : f"CR '{number}' not found"}, 404
        
class CRList(Resource):
    def get(self, name):
        customer = CustomerModel.find_by_name(name)
        return {'crs': [cr.toJson() for cr in customer.crs]}