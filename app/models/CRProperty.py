from app import db

class CRProperty(db.Model):
    """A fact about an animal."""

    __tablename__ = "cr_properties"

    cr_id = db.Column(db.ForeignKey("crs.id"), primary_key=True)
    key = db.Column(db.String(80), primary_key=True)
    value = db.Column(db.String(180))
