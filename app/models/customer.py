from app import db
from sqlalchemy_utils import aggregated
from app.models.cr import CRModel

class CustomerModel(db.Model):
    __tablename__ = "customers"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    cr_template = db.Column(db.String(32), nullable=False)
    protocol_template = db.Column(db.String(32), nullable=False)
    cr_folder = db.Column(db.String(32), nullable=False)
    service_ratio = db.Column(db.Float(precision=2), nullable=False )
    
    @aggregated('crs', db.Column(db.Integer))
    def last_cr_number(self):
        return db.func.max(CRModel.number)

    crs = db.relationship('CRModel', lazy='dynamic', cascade='all, delete-orphan')


    def __init__(self, name, cr_template, protocol_template, cr_folder, service_ratio):
        self.name = name
        self.cr_template = cr_template
        self.protocol_template = protocol_template
        self.cr_folder = cr_folder
        self.service_ratio = service_ratio

    def save_or_update(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def toJson(self):
        return {
            'id': self.id,
            'name':self.name,
            'cr_template':self.cr_template,
            'protocol_template':self.protocol_template,
            'cr_folder':self.cr_folder,
            'service_ratio':self.service_ratio,
            'crs':[cr.toJson() for cr in self.crs.all()]
            }

    @classmethod
    def find_by_name(cls, name):
        return cls.query.filter_by(name=name).first()