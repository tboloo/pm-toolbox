from app import db
from sqlalchemy.types import PickleType
import json
from app.utils.ProxiedDictMixin import ProxiedDictMixin
from sqlalchemy.orm import relationship
from sqlalchemy.orm.collections import attribute_mapped_collection
from sqlalchemy.ext.associationproxy import association_proxy
from app.models.CRProperty import CRProperty


class CRModel(ProxiedDictMixin, db.Model):
    __tablename__ = 'crs'
    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.String(80), nullable=False, unique=True)
    title = db.Column(db.String(240), nullable=False)
    issue_date = db.Column(db.Date, nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id'))
    folder_id = db.Column(db.String(32))
    file_id = db.Column(db.String(32))
    net_value = db.Column(
        db.Float(precision=4, as_decimal=True), nullable=False)
    net_service_base = db.Column(
        db.Float(precision=4, as_decimal=True), nullable=False)
    net_service_charge = db.Column(
        db.Float(precision=4, as_decimal=True), nullable=False)
    customer = db.relationship('CustomerModel')
    properties = relationship(
        "CRProperty", collection_class=attribute_mapped_collection("key"), cascade="all, delete-orphan"
    )

    _proxied = association_proxy(
        "properties",
        "value",
        creator=lambda key, value: CRProperty(key=key, value=value),
    )

    def __init__(self, number, title, issue_date, file_id, folder_id, customer_id, net_value, net_service_base, net_service_charge, properties_bag={}):
        self.number = number
        self.title = title
        self.issue_date = issue_date
        self.customer_id = customer_id
        self.file_id = file_id
        self.folder_id = folder_id
        self.net_value = net_value
        self.net_service_base = net_service_base
        self.net_service_charge = net_service_charge
        for k,v in properties_bag.items():
            self[k] = v

    def __repr__(self):
        return f'CR {self.number} - {self.title}, issued on {self.issue_date} for {getattr(self.customer, "name", None)}'

    # Needs to be defined since ProxiedDictMixin defines __len__ which evaluates to false for cr with no custom attributes
    # resulting in non executing if cr: ... statement
    def __bool__(self):
        return True

    def save_or_update(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def toJson(self):
        return {
            'id': self.id,
            'number': self.number,
            'title': self.title,
            'issue_date': self.issue_date.strftime('%Y%m%d'),
            'issued_for': getattr(self.customer, 'name', None),
            'folder_id': self.folder_id,
            'file_id': self.file_id,
            'net_value': self.net_value,
            'net_service_charge': self.net_service_charge,
            'properties': json.dumps(dict(self._proxied))
        }

    @classmethod
    def find_by_number(cls, number):
        return cls.query.filter_by(number=number).first()

    @classmethod
    def with_properties(self, key, value):
        return self.properties.any(key=key, value=value)
