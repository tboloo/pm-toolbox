import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import { Provider } from 'react-redux';
import AppRouter from './components/AppRouter';
import store from './store/configureStore';
import 'materialize-css/dist/css/materialize.min.css'

const title = 'PM Toolbox';

Modal.setAppElement('#app');

const jsx = (
    <Provider store={store}>
        <AppRouter title={title} />
    </Provider>
)

ReactDOM.render(jsx, document.getElementById('app'));
