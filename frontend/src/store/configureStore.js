import { createStore, combineReducers, applyMiddleware } from 'redux';
import customersReducer from '../reducers/customers';
import filtersReducer from '../reducers/filters';
import thunk from "redux-thunk";

const store = createStore(
    combineReducers({
        customers: customersReducer,
        filters: filtersReducer
    }),
    applyMiddleware(thunk)
);

export default store;