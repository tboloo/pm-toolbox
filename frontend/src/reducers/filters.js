import { ACTIONS } from '../utils/actions';

const filtersDefaultState = {
    name: '',
    sortBy: []
}

export default (state = filtersDefaultState, action) => {
    switch (action.type) {
        case ACTIONS.FILTER_BY_NAME:
            return {
                ...state,
                name: action.name
            };

        default:
            return state;
    }
};