import { ACTIONS } from '../utils/actions';

const customersReducerDefaultState = {
    selectedCustomer: undefined,
    customers: [],
    loading: false,
    api_error: undefined
};

export default (state = customersReducerDefaultState, action) => {
    switch (action.type) {
        case ACTIONS.GET_CUSTOMERS_FILTERED:
            return {
                ...state,
                customers: action.customers,
                loading: false
            }

        case ACTIONS.CUSTOMER_OPERATION_STARTED:
            return {
                ...state,
                loading: true
            }

        case ACTIONS.CUSTOMER_OPERATION_ERROR:
            return {
                ...state,
                api_error: action.api_error,
                loading: false
            }

        case ACTIONS.ADD_CUSTOMER:
            return {
                ...state,
                customers: [...state.customers, action.customer],
                loading: false,
                api_error: undefined
            }

        case ACTIONS.REMOVE_CUSTOMER:
            return {
                ...state,
                selectedCustomer: undefined,
                customers: state.customers.filter((c) => c.id !== action.id)
            }

        case ACTIONS.SELECT_CUSTOMER:
            return {
                ...state,
                selectedCustomer: action.selectedCustomer
            }

        case ACTIONS.EDIT_CUSTOMER:
            return {
                ...state,
                selectedCustomer: action.selectedCustomer,
                customers: state.customers.map((c) => {
                    if (c.id === action.id) {
                        return {
                            ...c,
                            ...action.updates
                        }
                    } else
                        return c
                }),
                loading: false,
                api_error: undefined
            }
        
        case ACTIONS.RESET_API_ERROR:
            return {
                ...state,
                loading: false,
                api_error: undefined
            }

        default:
            return state;
    }
};