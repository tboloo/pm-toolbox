import React from 'react';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import M from "materialize-css";
import { timingSafeEqual } from 'crypto';

class CustomerModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.customer ? this.props.customer.id : '',
            name: this.props.customer ? this.props.customer.name : '',
            cr_template: this.props.customer ? this.props.customer.cr_template : '',
            protocol_template: this.props.customer ? this.props.customer.protocol_template : '',
            cr_folder: this.props.customer ? this.props.customer.cr_folder : '',
            service_ratio: this.props.customer ? this.props.customer.service_ratio : ''
        }
    }

    componentDidUpdate() {
        M.updateTextFields();
    }

    handleSaveCustomer = (e) => {
        e.preventDefault();
        this.props.handleSaveCustomer(this.state);
    }

    onNameChange = (e) => {
        const name = e.target.value;
        this.setState(() => ({ name }));
    };

    onCRTemplateChange = (e) => {
        const cr_template = e.target.value;
        this.setState(() => ({ cr_template }));
    };

    onProtocolTemplateChange = (e) => {
        const protocol_template = e.target.value;
        this.setState(() => ({ protocol_template }));
    };

    onCRFolderChange = (e) => {
        const cr_folder = e.target.value;
        this.setState(() => ({ cr_folder }));
    };

    onServiceRatioChange = (e) => {
        const service_ratio = e.target.value;
        if (service_ratio.match(/^\d{0,}(,\d{0,2})?$/)) {
            this.setState(() => ({ service_ratio }));
        }
    };

    onAfterOpen = () => {
        this.setState(() => ({
            id: this.props.customer ? this.props.customer.id : '',
            name: this.props.customer ? this.props.customer.name : '',
            cr_template: this.props.customer ? this.props.customer.cr_template : '',
            protocol_template: this.props.customer ? this.props.customer.protocol_template : '',
            cr_folder: this.props.customer ? this.props.customer.cr_folder : '',
            service_ratio: this.props.customer ? this.props.customer.service_ratio : ''
        }))
    }

    render() {
        return (
            <Modal
                isOpen={this.props.isCustomerFormOpen}
                onRequestClose={this.props.handleClose}
                contentLabel="Add Customer"
                style={this.props.style}
                onAfterOpen={this.onAfterOpen}
            >

                <div className="row">
                    <form onSubmit={this.handleSaveCustomer} className='col s12'>
                        <div className="row">
                            <div className="input-field col s12">
                                <label htmlFor="customer" className="active">Klient: </label>
                                <input type="text" name="customer" value={this.state.name} disabled={this.props.customer} onChange={this.onNameChange} className={`${this.props.error && this.props.error.name && "invalid"}`} />
                                {this.props.error && this.props.error.name && <span className="helper-text" data-error={this.props.error.name}></span>}
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <label htmlFor="cr_template" className="active">Wzór CR</label>
                                <input name="cr_template" type="text" value={this.state.cr_template} onChange={this.onCRTemplateChange} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <label htmlFor="protocol_template" className="active">Wzór protokołu</label>
                                <input name="protocol_template" type="text" value={this.state.protocol_template} onChange={this.onProtocolTemplateChange} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <label htmlFor="cr_folder" className="active">Folder CR</label>
                                <input name="cr_folder" type="text" value={this.state.cr_folder} onChange={this.onCRFolderChange} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="input-field col s12">
                                <label htmlFor="service_ratio" className="active">Stawka serwisowa</label>
                                <input name="service_ratio" type="text" value={this.state.service_ratio} onChange={this.onServiceRatioChange} className={`${this.props.error && this.props.error.service_ratio && "invalid"}`} />
                                {this.props.error && this.props.error.service_ratio && <span className="helper-text" data-error={this.props.error.service_ratio}></span>}
                            </div>
                        </div>
                        <div className="row">
                            <button type="button" className="waves-effect waves-light btn" onClick={this.props.handleClose}>Anuluj</button>
                            <button className="waves-effect waves-light btn">Zapisz</button>
                        </div>
                        {this.props.loading &&
                            <div className="row">
                                <div className="progress col s12">
                                    <div className="indeterminate"></div>
                                </div>
                            </div>
                        }
                        {this.props.error && this.props.error.message &&
                            <div className="row">
                                <div className="col s12">
                                    <input name="error" disabled="true" value={this.props.error.message}
                                        className={`${this.props.error && this.props.error.message && "invalid"}`} />
                                </div>
                            </div>
                        }
                    </form>
                </div>
            </Modal >
        )
    }
};

const mapStateToProps = (state) => {
    return {
        customer: state.customers.selectedCustomer,
        error: state.customers.api_error,
        loading: state.customers.loading
    };
};

export default connect(mapStateToProps)(CustomerModal);