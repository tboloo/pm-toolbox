import React from 'react';

const Dashboard = (props) => {
    const divStyle = {
        padding: "35px"
    };

    return (
        <main>
            <div className="row">
                <div className="col s12">
                    <div style={divStyle} align="center" className="card">
                        <div className="row">
                            <div className="left card-title">
                                <b>User Management</b>
                            </div>
                        </div>

                        <div className="row">
                            <a href="/customers">
                                <div style={divStyle} className="grey lighten-3 col s2 waves-effect">
                                    <i className="indigo-text text-lighten-1 large material-icons">people</i>
                                    <span className="indigo-text text-lighten-1"><h5>Customers</h5></span>
                                </div>
                            </a>
                            <div className="col s1">&nbsp;</div>

                            <a href="/crs">
                                <div style={divStyle} className="grey lighten-3 col s2 waves-effect">
                                    <i className="indigo-text text-lighten-1 large material-icons">people</i>
                                    <span className="indigo-text text-lighten-1"><h5>CRs</h5></span>
                                </div>
                            </a>
                            <div className="col s1">&nbsp;</div>
                            <a href="#!">
                                <div style={divStyle} className="grey lighten-3 col s2 waves-effect">
                                    <i className="indigo-text text-lighten-1 large material-icons">store</i>
                                    <span className="indigo-text text-lighten-1"><h5>Product</h5></span>
                                </div>
                            </a>
                            <div className="col s1">&nbsp;</div>
                            <a href="#!">
                                <div style={divStyle} className="grey lighten-3 col s2 waves-effect">
                                    <i className="indigo-text text-lighten-1 large material-icons">assignment</i>
                                    <span className="indigo-text text-lighten-1"><h5>Product</h5></span>
                                </div>
                            </a>
                            <div className="col s1">&nbsp;</div>
                        </div>
                    </div>
                    <div style={divStyle} align="center" className="card">
                        <div className="row">
                            <div className="left card-title">
                                <b>Product Management</b>
                            </div>
                        </div>
                        <div className="row">
                            <a href="#!">
                                <div style={divStyle} className="grey lighten-3 col s2 waves-effect">
                                    <i className="indigo-text text-lighten-1 large material-icons">store</i>
                                    <span className="indigo-text text-lighten-1"><h5>Product</h5></span>
                                </div>
                            </a>

                            <div className="col s1">&nbsp;</div>

                            <a href="#!">
                                <div style={divStyle} className="grey lighten-3 col s2 waves-effect">
                                    <i className="indigo-text text-lighten-1 large material-icons">image_search</i>
                                    <span className="indigo-text text-lighten-1"><h5>Orders</h5></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </main>

    )
}

export default Dashboard;