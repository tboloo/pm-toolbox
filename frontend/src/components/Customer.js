import React from 'react';
import { connect } from 'react-redux';
import { removeCustomer } from '../actions/customers';

const Customer = (props) => {
    return (
        <div>
            <img src={props.logo} alt="" className="circle" />
            <span className="title">{props.name}</span>
            <span className="new badge" data-badge-caption="custom caption">4</span>
            <p>
                <b>wzór CR:&nbsp;</b> {props.cr_template}<br />
                <b>wzór protokołu:&nbsp;</b> {props.protocol_template}<br />
                <b>folder CR:&nbsp;</b>{props.cr_folder}<br />
                <b>stawka serwisowa:&nbsp;</b>{props.service_ratio}
            </p>
        </div>
    )
}

export default connect()(Customer);