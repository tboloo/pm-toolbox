import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import CustomerList from './CustomerList';
import NotFountPage from './NotFound';
import Header from './Header';
import App from './PMToolbox';

export default (props) => (
    <BrowserRouter>
        <div>
            <Header title={props.title}/>
            <Switch>
                <Route path="/" component={App} exact={true} />
                <Route path="/customers" component={CustomerList} />
                <Route component={NotFountPage} />
            </Switch>
        </div>
    </BrowserRouter>
)