import React from 'react';
import { connect } from 'react-redux';
import Customer from './Customer';
import { saveOrUpdateCustomer } from '../actions/customers';
import { selectCustomer } from '../actions/customers';
import CustomerModal from './CustomerModal';
import CustomerFilter from './CustomerFilter';
import selectCustomers from '../utils/filters';
import { getCustomersFiltered } from '../actions/customers';
import { resetApiError } from '../actions/customers';


class CustomerList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isCustomerFormOpen: false,
        }
    }

    componentDidMount() {
        this.props.dispatch(getCustomersFiltered());
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.loading && !this.props.loading &&!this.props.api_error)
        this.setState(() => {
            return {
                isCustomerFormOpen: false
            };
        });
    }

    handleNewCustomer = () => {
        this.props.dispatch(selectCustomer(undefined));
        this.setState(() => {
            return {
                isCustomerFormOpen: true
            }
        });
    }

    handleSaveCustomer = (customer) => {
        this.props.dispatch(resetApiError());
        this.props.dispatch(saveOrUpdateCustomer(customer));
    }

    handleClose = () => {
        this.props.dispatch(resetApiError());
        this.setState(() => {
            return {
                isCustomerFormOpen: false
            }
        });
    }

    handleEditCustomer = (customer) => {
        this.props.dispatch(selectCustomer(customer));
        this.setState(() => {
            return {
                isCustomerFormOpen: true,
            }
        });
    };

    render() {
        const customStyle = {
            overlay: { zIndex: 9999 }
        };
        const cursorStyle = {
            cursor: 'pointer'
        };

        return (
            <div>
                <h3 className="title">Klienci</h3>
                {this.props.loading ? <h2>Loading...</h2> : null}
                <CustomerFilter />
                <ul className="collection">
                    {
                        this.props.customers &&
                        this.props.customers.map((c) => {
                            return (
                                <li className="collection-item avatar" key={c.id} onClick={() => this.handleEditCustomer(c)} style={cursorStyle}>
                                    <Customer {...c} />
                                </li>
                            )
                        })
                    }
                </ul>
                <CustomerModal
                    isCustomerFormOpen={this.state.isCustomerFormOpen}
                    handleSaveCustomer={this.handleSaveCustomer}
                    handleClose={this.handleClose}
                    style={customStyle}
                    selectedCustomer={this.state.selectedCustomer}
                />
                <button className="waves-effect waves-light btn" onClick={this.handleNewCustomer}>Dodaj</button>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        customers: selectCustomers(state.customers.customers, state.filters),
        selectedCustomer: state.customers.selectedCustomer,
        loading: state.customers.loading,
        api_error: state.customers.api_error
    };
};

const ConnectedCustomerList = connect(mapStateToProps)(CustomerList);

export default ConnectedCustomerList;