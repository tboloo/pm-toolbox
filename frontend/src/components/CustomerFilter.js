import React from 'react';
import { connect } from 'react-redux';
import {filterByName} from '../actions/filters';

const CustomerFilter = (props) => (
    <div>
        <label>Szukaj:</label>
        <input type='text' onChange={(e) => {
            props.dispatch(filterByName(e.target.value))
        }}/>
    </div>
);

const mapStateToProps = (state) => {
    return {
        filters: state.filters
    }
}

export default connect(mapStateToProps)(CustomerFilter);