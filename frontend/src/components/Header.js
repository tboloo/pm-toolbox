import React from 'react';
import { Link } from 'react-router-dom';

const Header = (props) => {
    const searchInputStyle = {
        width: "400px",
        left: "400px"
    };

    return (
        <header>
            <nav>
                <div className="nav-wrapper">
                    <Link to="/" className="brand-logo">{props.title}</Link>
                    <ul id="nav-mobile" className="right hide-on-med-and-down">
                        <li><a href="#">Profile</a></li>
                    </ul>
                    <form className=" hide-on-med-and-down" id="form1" >
                        <div className="input-field" style={searchInputStyle}>
                            <input id="search" type="search" required />
                            <label className="label-icon" htmlFor="search"><i className="material-icons">search</i></label>
                            <i className="material-icons">close</i>
                            <div id="searchResults" ></div>
                        </div>
                    </form>
                </div>
            </nav>
        </header>
    );
}

export default Header;