import uuid from 'uuid/v4';
import { ACTIONS } from '../utils/actions';
import axios from 'axios';
import store from '../store/configureStore';

export const getCustomersFiltered = (limit, skip, filters) => {
    return dispatch => {
        store.dispatch(customerOperationStarted());

        axios
            .get('http://127.0.0.1:5000/api/v1/customers')
            .then(res => {
                dispatch(filteredCustomersFetchedSuccess(res.data.customers))
            })
            .catch(err => {
                dispatch(handleError(err.message));
            });
    };
};

export const saveOrUpdateCustomer = customer => {
    return dispatch => {
        store.dispatch(customerOperationStarted());
        customer.id ? 
            axios.put(`http://127.0.0.1:5000/api/v1/customer/${customer.name}`, customer) 
            .then(res => {
                //dispatch(editCustomer(res.data.id, res.data));
                setTimeout(() => {
                    dispatch(editCustomer(res.data.id, res.data));
                  }, 2500);
            })
            .catch(err => {
                dispatch(handleError(err.response ? err.response.data.message : {'message': err.message}));
            })
            :
            axios.post(`http://127.0.0.1:5000/api/v1/customer/${customer.name}`, customer)
            .then(res => {
                dispatch(addCustomer(res.data));
            })
            .catch(err => {
                dispatch(handleError(err.response ? err.response.data.message : {'message': err.message}));
            })
    }
};

export const filteredCustomersFetchedSuccess = customers => ({
    type: ACTIONS.GET_CUSTOMERS_FILTERED,
    customers
});

export const customerOperationStarted = () => ({
    type: ACTIONS.CUSTOMER_OPERATION_STARTED
});

export const addCustomer = (
    {
        id = '',
        name = '',
        cr_template = '',
        protocol_template = '',
        cr_folder = '',
        service_ratio = ''
    } = {},
) => ({
    type: ACTIONS.ADD_CUSTOMER,
    customer: {
        id,
        name,
        cr_template,
        protocol_template,
        cr_folder,
        service_ratio
    }
});

export const handleError = api_error => ({
    type: ACTIONS.CUSTOMER_OPERATION_ERROR,
    api_error
});

export const removeCustomer = (id) => ({
    type: ACTIONS.REMOVE_CUSTOMER,
    id
});

export const editCustomer = (id, updates) => ({
    type: ACTIONS.EDIT_CUSTOMER,
    id,
    updates
});

export const selectCustomer = (customer) => ({
    type: ACTIONS.SELECT_CUSTOMER,
    selectedCustomer: customer
});

export const resetApiError = () => ({
    type: ACTIONS.RESET_API_ERROR
})