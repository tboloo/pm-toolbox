import { ACTIONS } from '../utils/actions';

export const filterByName = (name = '') => ({
    type: ACTIONS.FILTER_BY_NAME,
    name
});