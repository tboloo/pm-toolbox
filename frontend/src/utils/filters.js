
export default (customers, {name}) => {
    return name === '' ?  customers : customers.filter((c) => {
        return c.name.toLowerCase().includes(name.toLowerCase());
    });
}